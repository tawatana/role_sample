role_sample
=========

roleのサンプル実装

Requirements
------------

特になし

Role Variables
--------------

defaults/main.yml に記載

Dependencies
------------

特になし

Example Playbook
----------------

```
---
- hosts: web_servers_sample
  roles:
     - role_sample
```

License
-------

BSD

Author Information
------------------

特になし
